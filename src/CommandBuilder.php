<?php
namespace dbx12\CommandBuilder;
class CommandBuilder {

    private $commands = [];

    public function CommandBuilder(){}

    /**
     * Adds a token to the command stack
     * @param string $token the token
     * @param string $fingerprint calculated finger print of the token
     * @param string $createdAt creation time (Y-m-d H:i:s formatted)
     * @return $this
     */
    public function addToken($token, $fingerprint, $createdAt){
        $command = [
            'command' => 'ADD_TOKEN',
            'token' => $token,
            'fingerprint' => $fingerprint,
            'createdAt' => $createdAt
        ];
        $this->commands[]=$command;
        return $this;
    }

    /**
     * Set certificate from file at given path
     * @param string $certPath path to certificate
     * @return $this
     */
    public function setCertificate($certPath){
        
        if(!is_readable($certPath)){
            throw new \Exception("Certificate at $certPath is not readable");
        }
        
        $cert = file($certPath);
        $cert = implode('', $cert);
        //TODO concatenate the whole string or must I keep the newlines?
        
        $command = [
            'command' => 'SET_CERTIFICATE',
            'certificate' => $cert
        ];
        $this->commands[] = $command;
        return $this;
    }

    /**
     * Set configuration parameters (IP address / hostname)
     * @param string $endpoint endpoint of the portal (e.g. 192.168.2.111/doorfriend/open)
     * @return $this
     */
    public function setConfiguration($endpoint){
        $command = [
            'command' => 'SET_CONFIG',
            'endpoint' => $endpoint,
        ];
        $this->commands[] = $command;
        return $this;
    }

    /**
     * Add a door to the command stack
     * @param int $id the id of the door
     * @param string $displayname the displayname of the door
     * @return $this
     */
    public function addDoor($id,$displayname){
        $command = [
            'command' => 'ADD_DOOR',
            'id' => $id,
            'displayname' => $displayname,
        ];
        $this->commands[] = $command;
        return $this;
    }
    
    /**
     * Create JSON from the command stack
     * @return string JSON-formatted command stack
     */
    public function write(){
        return json_encode($this->commands);
    }

    /**
     * Return the raw command stack
     * @return array
     */
    public function raw(){
        return $this->commands;
    }
}
